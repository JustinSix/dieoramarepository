﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //variables
    public float Speed = 10f;
    public Animator anim;


    // Use this for initialization
    void Start () {
        anim = gameObject.GetComponent<Animator>();
	}

    // Update is called once per frame
    void Update() {
        //walk
        //store user input 
        float Dirx = Input.GetAxis("Horizontal") * Speed * Time.deltaTime;
        //move according to user input
        transform.position = new Vector2(transform.position.x + Dirx, transform.position.y);

       



        //check if player is moving and play correct animation
        if (GetComponent<Rigidbody2D>().velocity.magnitude > 0)
        {
            anim.SetFloat("speed", 1);
        }
       else
        {
            anim.SetFloat("speed", 0);
        }
	}
}
