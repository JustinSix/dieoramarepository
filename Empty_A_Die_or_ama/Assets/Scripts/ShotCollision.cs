﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotCollision : MonoBehaviour {

    public Animator anim3;
    public GameObject glass;
    void OnCollisionEnter2D(Collision2D co2)
    {
        if (co2.gameObject.name == "heartcharacter")
        {
            anim3.SetBool("shotglassanimation", true);
            Destroy(glass.gameObject, 1.0f);
        }
    }
   // private IEnumerator Shatter()
   // {
   //         yield return new WaitForSeconds(3.0f);

   //         Destroy(glass.gameObject);
   // }
}
