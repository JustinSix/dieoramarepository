﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour {

    public Animator anim2;

    void OnCollisionEnter2D(Collision2D co1)
    {
        if(co1.gameObject.name == "shotglass")
        {
            anim2.SetBool("IsShirtless", true);
        }
    }

}
